var pm = null;
var startDate = null;
var avgSpeed = 0;
var transactions_count = 0;
var transactions_done = 0;



jQuery(function() {
    $('#mainform').submit(function(e) {
        e.preventDefault()

        pm = new PerfectMoneyMasspay(
            $('#AccountID').val(),
            $('#PassPhrase').val(),
            $('#Wallet').val()
        );
       
        pm.on('error', function(str) {
            alert(str);
            $('#pause').text('Retry');
            //$('#pay').text('Retry');

        }).on('done', function() {
            $('#pause').hide();
            $('#export').show();
            alert('Done');

        }).on('transaction-start', function(transObj) {
            var row = findRow(transObj.id);
            row.removeClass('error');
            row.removeClass('complete');
            row.addClass('in_progress').find('info').text('In progress');

        }).on('transaction-end', function(transObj) {
            var row = findRow(transObj.id);
            row.removeClass('in_progress');
            if (transObj.batch > 0)
            {
                row.addClass('complete');
                row.find('info').text(transObj.batch);
            }
            else
            {
                row.addClass('error');
                row.find('info').text(transObj.error);
            }

            transactions_done++;
            $('#transactions_remaining').text(transactions_count-transactions_done)

        });

        
        var textarea = $('#csv');
        var lines = textarea.val().split(/\r\n|\r|\n/g);

        transactions_count = 0;

        for (i in lines)
        {
            var line = lines[i].trim();
            // Skip empty lines
            if (!line)
            {
                continue;
            }

    		var values = $.csv2Array(line)[0];

            // Check CSV string parsed and first item is Account ID, 2nd - amount
            if (!values || !values[0].match(/^U(\d+)$/) || !values[1].match(/(\d+\.?\d*)/))
            {
                alert("Unable to parse line \""+line+"\"");
                textareaGoToLine(textarea, i);
                return false;
            }

            pm.add(i, values[0], values[1], values[2], values[3]);
            addRow(i, values[0], values[1], values[2], values[3]);
            transactions_count++;
        }

        if (!confirm("Are you sure you want to start payout?"))
        {
            return false;
        }

        // Adjust interface
        $('.form').hide();
        $('.results').show();
        $('#pay').hide();
        $('#pause').show();
        $('#transactions_count').text(transactions_count);
        $('#transactions_remaining').text(transactions_count);

        pm.pay();
        startTimer();
    });

    

    $('#pause').click(function() {
        if (typeof pm == "undefined")
        {
            return false;
        }

        var paused = pm.pause(),
            caption = (paused)  ?  'Resume'  :  'Pause';

        $('#pause').val(caption);
    });

    $('#export').click(function() {
        var rows = $('#results').find('row'),
            success_head = "SUCCESSFUL TRANSACTIONS\n",
            errors_head  = "\nFAILED TRANSACTIONS\n";
            
        $('#csv_result')
            .val(success_head+rowsToCSV(rows.filter('.complete'))+errors_head+rowsToCSV(rows.filter('.error')))
            .show();
    });
});


function textareaGoToLine(textarea, num)
{
    var lineHeight = parseInt(textarea.css('line-height'));
    textarea.scrollTop(num * lineHeight);
}

function addRow(id, recipient, amount, memo, merchant)
{
    if (!merchant) merchant = ""

    return $('<row id="row'+id+'"><to>'+recipient+'</to><amount>'+amount+'</amount><memo>'+memo+'</memo><merch>'+merchant+'</merch><info></info></row>')
        .appendTo($('#results'));
}

function findRow(id)
{
    return $('#results').find('#row'+id);
}

function startTimer()
{
    startDate = new Date();
   
    $('#timer').text(startDate.toLocaleTimeString());
    $('#speed').text('0');
    $('#remaining').text('???');
    $('#timebar').show();
}

function rowsToCSV(elements)
{
    return elements.map(function(i) {
        console.log("t", this);
        var acc = $(this).find('to').text(),
            amount = $(this).find('amount').text(),
            memo = $(this).find('memo').text(),
            merch = $(this).find('merch').text(),
            info = $(this).find('info').text();

        return '"'+acc+'","'+amount+'","'+memo+'","'+info+'"';
    }).get().join("\n");
}