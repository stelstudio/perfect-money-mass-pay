function PerfectMoneyMasspay(AccountID, PassPhrase, Wallet)

{
    var self = this,
    	url = 'proxy.php',
        //url = 'test.php',
    	settings = {
    		maxProgress: 1
    	};

    // List of transactions
    var transactionsQueue = new Array();

    // Processed transactions
    var transactionsResult = new Array();

    // Transactions in progress
    var inProgress = 0;

    // Paused or running
    var paused = false;

    // Is error
    errorMsg = false;

    var callbacks = {
        transactionStart: function(transObj) { console.log('transactionStart', transObj); }, // Before transaction is executed
        transactionEnd: function(transObj) { console.log('transactionEnd', transObj); }, // On successful transaction
        error: function(msg) { console.log('error', msg); }, // On connection error
        done: function() { console.log('done'); } // Everything done
    }

    /**
     * Add transactions to queue
     */
    self.add = function(id, recipient, amount, memo, merchant)
    {
        transactionsQueue.push({
            id: id, // Internal id
            recipient: recipient,
            amount: amount,
            memo: memo,
            merchant: merchant
        });
    };

    
    /**
     * Start/keep payments
     */
    self.pay = function()
    {
        if (paused) return;

        while (inProgress < settings.maxProgress && transactionsQueue.length > 0 && !errorMsg)
        {
            post(transactionsQueue.shift());
        }

        if (!transactionsQueue.length && !inProgress)
        {
            callbacks.done();
        }
    };

    /**
     * Pause or resume
     */
    self.pause = function()
    {
        paused = !paused;
        self.pay();
        return paused;
    }
    
    /**
     * Send https request
     */
    var post = function(transObj)
    {
        var data = {
            AccountID: AccountID,
            PassPhrase: PassPhrase,
            Payer_Account: Wallet,
            Payee_Account: transObj.recipient,
            Amount: transObj.amount,
            Memo: transObj.memo,
            PAYMENT_ID: transObj.merchant
        };

        callbacks.transactionStart(transObj);

        ++inProgress;

        $.post(url, data).done(function(response) {
            responseObj = new pmResponse(response); 
            transObj.error = responseObj.field('ERROR');
            transObj.batch = responseObj.field('PAYMENT_BATCH_NUM');
            transObj.name  = responseObj.field('Payee_Account_Name'); 
            transactionsResult.push(transObj);

            callbacks.transactionEnd(transObj);

            --inProgress;

            // Next
            self.pay();

        }).fail(function(error) {
            transObj.error = "Connection error";
            callbacks.transactionEnd(transObj);
            // Return transaction back to the queue (at the beginning)
            transactionsQueue.unshift(transObj);

            // Do not overwrite existing message because it will cause multiple events
            if (!errorMsg)
            {
                errorMsg = "Unable to connect to "+url;
                callbacks.error(errorMsg);
            }

            --inProgress;
        });
    }

       

    /**
     * Attach callbacks
     */
    self.on = function(event, callback) {
        if (event == 'error')
        {
            callbacks.error = callback;
        }
        else if (event == 'transaction-start')
        {
            callbacks.transactionStart = callback;
        }
        else if (event == 'transaction-end')
        {
            callbacks.transactionEnd = callback;
        }
        else if (event == 'done')
        {
            callbacks.done = callback;
        }

        return this;
    }

}



function pmResponse(data)

{
    var html = $(data);
    this.field = function(name)
    {
        return html.filter('input[type="hidden"][name="'+name+'"]').val();
    }
}